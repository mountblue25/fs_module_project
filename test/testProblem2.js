
let {fs, convertingToUpper, convertingToLower, sortingTheLines} = require('../problem2')

fs.readFile('../lipsum.txt', 'utf8', function(error, data) {
    if (error) {
        throw error
    } else{
        convertingToUpper(data, ()=>{
            convertingToLower( () =>{
                sortingTheLines(() => {
    
                })
            })
        })
    }
});